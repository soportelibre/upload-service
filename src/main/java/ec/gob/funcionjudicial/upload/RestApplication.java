package ec.gob.funcionjudicial.upload;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configuración de la aplicacion REST.
 * 
 * @author Ricardo Arguello <ricardo.arguello@soportelibre.com>
 */
@ApplicationPath("/rest")
public class RestApplication extends Application {
}