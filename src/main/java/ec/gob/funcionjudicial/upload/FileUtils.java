package ec.gob.funcionjudicial.upload;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase utilitaria para manejo de archivos.
 * 
 * @author Ricardo Arguello <ricardo.arguello@soportelibre.com>
 */
public class FileUtils {

    private static final String PREFIX = "upload-";
    private static final String SUFFIX = ".pdf";

    /**
     * Guarda un archivo (InputStream) dentro de un directorio.
     * 
     * @param in
     * @param directory
     * @return
     * @throws IOException
     */
    public static String saveFile(InputStream in, String directory) throws IOException {
        // Crea un archivo con nombre randomico dentro del directorio
        Path file = Files.createTempFile(Paths.get(directory), PREFIX, SUFFIX);

        // Copia el InputStream al archivo
        Files.copy(in, file, StandardCopyOption.REPLACE_EXISTING);

        // Retorna el nombre del archivo creado
        return file.getFileName().toString();
    }

    /**
     * Lista los archivos dentro de un directorio.
     * 
     * @param directory
     * @return
     * @throws IOException
     */
    public static List<String> getFileNames(String directory) throws IOException {
        List<String> fileNames = new ArrayList<>();

        // Itera en los archivos dentro del directorio
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(directory))) {
            for (Path file : stream) {
                fileNames.add(file.getFileName().toString());
            }
        }

        return fileNames;
    }

    /**
     * Obtiene el contenido de un archivo dentro de un directorio.
     * 
     * @param directory
     * @param fileName
     * @return
     * @throws IOException
     */
    public static byte[] getFile(String directory, String fileName) throws IOException {
        return Files.readAllBytes(Paths.get(directory, fileName));
    }
}