package ec.gob.funcionjudicial.upload;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

/**
 * Servicio REST para carga y descarga de archivos.
 * 
 * @author Ricardo Arguello <ricardo.arguello@soportelibre.com>
 */
@Path("/files")
public class FileService {

    // Directorio en donde guardar los archivos
    private static final String UPLOAD_DIRECTORY = "/mnt/gfs";

    // Nombre del campo multipart/form-data en el formulario
    private static final String FORM_PARAMETER = "archivo";

    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject uploadFile(MultipartFormDataInput input) {
        // Para construir un array de archivos
        JsonArrayBuilder builder = Json.createArrayBuilder();

        // Obtener los archivos del formulario multipart/form-data
        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
        List<InputPart> inputParts = uploadForm.get(FORM_PARAMETER);

        // Iterar en cada archivo enviado
        for (InputPart inputPart : inputParts) {
            try {
                InputStream in = inputPart.getBody(InputStream.class, null);
                String fileName = FileUtils.saveFile(in, UPLOAD_DIRECTORY);
                builder.add(Json.createObjectBuilder().add("nombre", fileName));
            } catch (IOException e) {
                throw new ServerErrorException(Response.Status.INTERNAL_SERVER_ERROR, e);
            }
        }

        JsonArray archivos = builder.build();
        return Json.createObjectBuilder().add("archivos", archivos).build();
    }

    @GET
    @Path("/list")
    public JsonObject listFiles() {
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

        try {
            List<String> fileNames = FileUtils.getFileNames(UPLOAD_DIRECTORY);

            for (String fileName : fileNames) {
                arrayBuilder.add(Json.createObjectBuilder().add("nombre", fileName));
            }
        } catch (IOException e) {
            throw new ServerErrorException(Response.Status.INTERNAL_SERVER_ERROR, e);
        }

        return Json.createObjectBuilder().add("archivos", arrayBuilder.build()).build();
    }

    @GET
    @Path("/download")
    public Response downloadFile(@QueryParam("name") String fileName) {
        try {
            byte[] file = FileUtils.getFile(UPLOAD_DIRECTORY, fileName);
            return Response.status(200).entity(file).build();
        } catch (IOException e) {
            throw new ServerErrorException(Response.Status.INTERNAL_SERVER_ERROR, e);
        }
    }

    @GET
    @Path("/download/{name}")
    public Response downloadFileWithPath(@PathParam("name") String fileName) {
        return downloadFile(fileName);
    }
}